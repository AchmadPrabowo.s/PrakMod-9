<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;

	class ArrayHasKeyTest extends TestCase {
		public function testFailure() {
			$this->assertArrayHasKey('bar', ['bar' => 'baz']);
		}
	}
?>