<?php
	namespace PHPUnit\Framework;
	use PHPUnit\Framework\TestCase;
	include "Testable.php";

	class TestableTest4 extends TestCase {
		private $_testable = null;

		public function setUp() {
			$this->_testable = new Testable();
		}

		public function tearDown() {
			$this->_testable = null;
		} 

		public function testArrayKeyExists() {
			$this->assertArrayHasKey('first key', $this->_testable->testArray);
		}

		public function testAttributeExists() {
			$this->assertClassHasAttribute('resetMe', get_class($this->_testable));
		}

		public function testFileIsReal() {
			$this->assertFileExists('TextFile.txt');
		}
	}
?>